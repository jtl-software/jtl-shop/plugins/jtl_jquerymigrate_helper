jQueryMigrate Plugin für JTL-Shop 5.x
=====================================

Dieses Plugin fügt dem JTL-Shop *jQueryMigrate* hinzu und unterstützt somit Entwickler bei der Anpassung
bestehender Lösungen (Plugins, Templates, usw.) an das, im JTL-Shop seit Version 4.7 verwendete, jQuery 3.x.

![Firefox Entwicklerwerkzeuge](jqm_scrcapt.png "Firefox Entwicklerwerkzeuge")

Mindestvoraussetzung für die Funktion von *jQueryMigrate* ist *jQuery* 1.12 oder 2.x.

__Dieses Plugin ist nicht für dauerhaften, produktiven Einsatz gedacht!__

*jQueryMigrate* sollte zum Aufspüren von Inkompatibilitäten der bestehenden *jQuery*-Lösung (1.12 oder 2.x) benutzt
werden und nach erfolgter Überarbeitung wieder deaktiviert werden, sodaß anschließend von einer *jQuery 3.x*
kompatiblen Applikation ausgegangen werden kann.

Aktuell beinhaltet dieses Plugin die *jQueryMigrate* Version 3.1.1-pre.

Weiterführende Links:
---------------------

* [jQuery Upgrade Guide](https://jquery.com/upgrade-guide/3.0/)
* [jQueryMigrate](https://github.com/jquery/jquery-migrate)
